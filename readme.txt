#1 instalar dependencias usando pip
*pip install opencv-contrib-python
*pip install imutils

#DESCRIPCION
hay tres scripts:
1.catchFaces.py 
almacena fotogramas de rostros del video que se le indique usando cv2.VideoCapture

los videos usados estan dentro de la carpeta raiz y estos fueron jhan-x.mp4 y kers-x.mp4

los fotogramas se almacenan en una carpeta creada con el nombre de la persona.

2.createModelRF.py 
crea modelos xml con la informacion de los fotogramas, usualmente probamos tres modelos:
EigenFaces, FisherFace, LBPHFace

imprime el recorrido de los directorios de los fotogramas usando cv2.imshow y muestra el numero de imagenes

personalmente nos parece mejor LBPHFace, sin embargo dejamos metodos de prueba comentados en el siguiente script por si se quieren probar

3.faceRecognize.py
en este script se guarda los paths de los fotogramas almacenados en el script 1, ademas se lee el modelo guardado en el script 2 (debe ser uno de los 3)

reproduce el video a aplicarle el reconocimiento se hace deteccion de rostro y se imprime debajo del rectangulo que detecta el rostro si el rostro esta dentro de los modelos almacenados o si es desconocido

para realizar las pruebas debe descomentar una de las instancias de creacion de modelo junto a la lectura del respectivo modelo

elegir uno de los videos de prueba

y descomentar el respestivo metodo de impresion de los resultados de reconocimiento facial.


HECHO POR: JHAN NEGRETE BADILLO & KERLYS NIETO FIGUEROA

